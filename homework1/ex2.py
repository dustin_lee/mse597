import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os

matplotlib.rc("font", **{"size": 16})


# The units are meters, kilograms and seconds. They are implicit throughout. 
mass = 1.
first_location = -0.001
second_location = 0.
max_time = 100.
step_size = 0.001
num_steps = int(max_time/step_size)
times = np.linspace(0., max_time, num_steps+2)
# The +2 is for first_location and second_location.


def get_potential(x):
    if x >= 0.:
        potential = (x-1.)**2.
    else:
        potential = (x+1.)**2.
    return potential


def get_acceleration(x):
    # This acceleration is from the given potential. 
    if x >= 0.:
        force = -2.*(x-1.)
    else:
        force = -2.*(x+1.)
    acceleration = force/mass
    return acceleration


def plot_potential():
    x = np.linspace(-5., 5., 1000)
    potentials = np.vectorize(get_potential)(x)
    accelerations = np.vectorize(get_acceleration)(x)
    plt.plot(x, potentials, label="potential")
    plt.plot(x, accelerations, label="acceleration")
    plt.legend()
    plt.show()
    return None


def step(prev_location, cur_location):
    # This is the Verlet scheme without velocities. 
    acceleration = get_acceleration(cur_location)
    next_location = 2*cur_location - prev_location + acceleration*step_size**2.
    return next_location


def run_simulation():
    prev_location = first_location
    cur_location = second_location
    locations = [first_location, second_location]
    for _ in range(num_steps):
        next_location = step(prev_location, cur_location)
        locations.append(next_location)
        temp = cur_location
        cur_location = next_location
        prev_location = temp
    return locations


def plot_locations(locations, label=None):
    # Make plot. 
    plt.plot(times, locations, label=label)
    plt.xlabel("time (s)", font={"size":16})
    plt.ylabel("coordinate (m)", font={"size":16})
    # Return. 
    return None


def plot_energies(locations, label=None):
    # Get the energies. 
    locations_ary = np.array(locations)
    times_ary = np.array(times)
    velocities = (locations_ary[2:] - locations_ary[:-2])/(2.*step_size)
    kinetic_energies = .5 * mass * velocities**2.
    potential_energies = mass*np.vectorize(get_potential)(locations_ary[1:-1])
    energies = kinetic_energies + potential_energies
    # Make the plot.
    plt.plot(times[1:-1], energies, label=label)
    plt.xlabel("time (s)", font={"size":16})
    plt.ylabel("total energy (J)", font={"size":16})
    # Return. 
    return None


def display_plot(file, has_legend=False):
    if has_legend:
        plt.legend()
    plt.tight_layout()
    plt.savefig("figures/" + file)
    plt.show()


def change_globals(new_step_size):
    # Introduce the globals. 
    global step_size
    global first_location
    global num_steps
    global times
    # Change them. 
    step_size = new_step_size
    first_location = -step_size
    num_steps = int(max_time/step_size)
    times = np.linspace(0., max_time, num_steps+2)
    return None


def main():
    # Make the directory for the figures. 
    os.system("mkdir figures 2> /dev/null")
    # Visualize the potential. 
    plot_potential()
    # Compute the the initial total energy (for Part 2).
    velocity = (second_location - first_location)/step_size
    kinetic_energy = .5 * mass * velocity**2
    middle_location = (second_location - first_location)/2.
    potential_energy = mass * get_potential(middle_location)
    total_energy = kinetic_energy + potential_energy
    print("The starting velocity of the particle "
          + f"is {round(velocity, 2)} m/s.")
    print("The starting potential energy of the particle"
          + f"is {round(potential_energy, 2)} J.")
    print("The starting total energy of the system "
          + f"is {round(total_energy, 2)} J.")
    # Make the plots of the locations and energies. 
    locations = run_simulation()
    plot_locations(locations)
    display_plot("ex2locations.jpg")
    plot_energies(locations)
    display_plot("ex2energies.jpg")
    # Now, make the plots for various step sizes. 
    for new_step_size in [.0001, .001, .01]:
        change_globals(new_step_size)
        locations = run_simulation()
        plot_locations(locations, label=f"step size: {new_step_size} m")
    display_plot("ex2locations2.jpg", has_legend=True)
    for new_step_size in [.0001, .001, .01]:
        change_globals(new_step_size)
        locations = run_simulation()
        plot_energies(locations, label=f"step size: {new_step_size} m")
    display_plot("ex2energies2.jpg", has_legend=True)
    
    # Return. 
    return None


if __name__ == "__main__":
    main()


