import itertools
import math as ma
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
import random as rand

matplotlib.rc("font", **{"size": 16})


mass = 1.
beta = 1.


def comp_potential(dist2):
    # This is the Lennard-Jones Potential. 
    potential = 4. * ((1.**2./dist2)**6. - (1.**2./dist2)**3.)
    return potential


class System:

    def __init__(self, initial_state=None):
        # System parameters. 
        cutoff_dist = .2
        self.cutoff_dist2 = cutoff_dist**2. 
        self.energy_ref = comp_potential(self.cutoff_dist2)
        # Set the state. 
        if initial_state is not None:
            self.state = initial_state
        else: 
            # Put the particles into a regularly arranged manner. 
            xcoords = np.linspace(.0, .9, 10)
            ycoords = np.linspace(.0, .9, 10)
            self.state = (np.dstack(np.meshgrid(xcoords, ycoords))
                            .squeeze().reshape((-1, 2)))
            # Randomly perturb the particles. 
            displacements = (np.random.uniform(-.05, .05, size=2*100)
                                .reshape((-1, 2)))
            self.state += displacements
        self.state %= 1. # Apply the periodic boundary condition. 
        # Compute the energy of the system and return. 
        self.comp_energy()
        return None
        
    def comp_energy(self):
        # This computes the energy using the Lennard-Jones potential. 
        # The displacements will be used for periodic boundary conditions. 
        displacements = [np.array([x, y])
                            for x, y in itertools.product([-1., 0., 1.], repeat=2)] 
        # Add the potential energies between pairs of atoms. 
        self.energy = 0.
        for i in range(0, 100):
            for j in range(i+1, 100):
                location1 = self.state[i,:]
                location2 = self.state[j,:]
                # Get distance between particles.
                for displacement in displacements:
                    location2b = location2 + displacement
                    dist2 = ((location2b - location1)**2).sum()
                    if dist2 < self.cutoff_dist2:
                        potential = comp_potential(dist2) - self.energy_ref
                        self.energy +=  mass * potential
        # Return
        return None

    def step(self):
        # This is the Metropolis algorithm. 
        # Randomly perturb the state. 
        particle_id = rand.randint(0, 99)
        displacement = np.random.uniform(-.002, .002, size=2)
        new_state = self.state.copy()
        new_state[particle_id, :] += displacement
        new_system = System(new_state)
        # Decide whether to change to the new state. 
        do_accept = False
        try: 
            minimum = min(1., ma.exp(-beta*(new_system.energy - self.energy)))
            acceptance_threshold = rand.random()
            accept = (acceptance_threshold < minimum)
        except:
            accept = True
        if accept:
            self.state = new_system.state
            self.energy = new_system.energy
        # Return. 
        return None


def simulate(num_steps):
    # Initialize the system. 
    system = System()
    energy_scale = abs(system.energy)
    # Run the simulation. 
    energies = [system.energy/energy_scale]
    states = [system.state]
    for i in range(0, num_steps):
        if i%100 == 0:
            print(f"The energy at step {i} is {system.energy/energy_scale}.")
        system.step()
        energies.append(system.energy/energy_scale)
        states.append(system.state)
    # Return. 
    return energies, states


def plot_energies(energies, step=1000):
    domain = list(range(0, step))
    energies = energies.copy()
    energies = energies[:step]
    plt.plot(domain, energies)
    plt.xlabel("number of steps")
    plt.ylabel("energy")
    plt.title(f"After {step} steps")
    plt.savefig(f"figures/ex3energies{step}.jpg")
    plt.show()
    return None


def plot_first_and_last_states(states, step=1000):
    plt.scatter(states[0][:,0], states[0][:,1], color="blue",
                label="original configuration")
    plt.scatter(states[step][:,0], states[step][:,1], color="red",
                label="final configuration")
    plt.title(f"After {step} steps")
    plt.legend()
    plt.savefig(f"figures/ex3states{step}.jpg")
    plt.show()
    return None


def main():
    # Make the directory for the figures. 
    os.system("mkdir figures 2> /dev/null")
    # Adjust the step size. 
    num_steps = 10000
    # Run the simulation. 
    energies, states = simulate(num_steps)
    print(f"The energy at step {num_steps} is {energies[-1]}.")
    # Plot. 
    plot_energies(energies, step=1000)
    plot_energies(energies, step=num_steps)
    plot_first_and_last_states(states, step=1000)
    plot_first_and_last_states(states, step=num_steps)
    # Return. 
    return None


if __name__ == "__main__":
    main()