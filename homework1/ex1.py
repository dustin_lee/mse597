import math as ma
import matplotlib
import matplotlib.pyplot as plt
import os
import random as rand

matplotlib.rc("font", **{"size": 16})


def compute_pi(sample_size=100):
    # Randomly sample points in the square and count
    # how many are in the circle. 
    num_within = 0 # Number within the circle.  
    for _ in range(sample_size):
        x_val = 2.*rand.random() - 1.
        y_val = 2.*rand.random() - 1.
        if x_val**2. + y_val**2. < 1.:
            num_within += 1
    # Compute the estimated area of the circle.
    # The are of the square that circumscribes the circle is 4. 
    pi = 4.*(num_within/sample_size) 
    # Estimate the standard deviation in the estimation of pi.
    num_without = sample_size - num_within
    sd2 = (num_within*(4. - pi)**2 + num_without*(0. - pi)**2)
    sd2 /= (sample_size - 1)
    sd = sd2**.5
    sdmean = sd / sample_size**.5
    # Return
    return pi, sdmean


def plot_estimates():
    # Get the data. 
    sample_sizes = []
    estimates = []
    lower_sd = []
    upper_sd = []
    for i in range(1, 7):
        sample_size = 10**i
        pi, sd = compute_pi(sample_size)
        sample_sizes.append(sample_size)
        estimates.append(pi)
        lower_sd.append(pi - sd)
        upper_sd.append(pi + sd)
    # Make the plots. 
    plt.plot(sample_sizes, estimates, color="blue", label="estimate")
    plt.plot(sample_sizes, lower_sd, color="red", linestyle="dashed",
             label="one standard deviation")
    plt.plot(sample_sizes, upper_sd, color="red", linestyle="dashed")
    plt.semilogx()
    plt.legend()
    plt.xlabel("sample size", font={"size":16})
    plt.ylabel("estimate", font={"size":16})
    # Show, save and return. 
    plt.tight_layout()
    plt.savefig("figures/ex1pi.jpg")
    plt.show()
    return None


def main():
    # Make the directory for the figures. 
    os.system("mkdir figures 2> /dev/null")
    # Adjust the sample size. 
    sample_size = 10**6
    # Run the simulation. 
    pi, sd = compute_pi(sample_size)
    # Print results. 
    print("The estimated value of Archimedes's constant by "
          + f"{sample_size} sample  is {round(pi, 6)} ± {round(sd, 6)}.")
    error = abs(pi - ma.pi)
    print("The error of this estimate from the true value "
          + f"is {round(error, 5)}.")
    # Plot. 
    plot_estimates()
    # Return. 
    return None


if __name__ == "__main__":
    main()

