import math as ma


def convert_energy(energy:"Ry"):
    factor = 2.179e-18
    return factor*energy


def comp_wave_num(e1, e2, dx):
    # Convert units. 
    e0 = convert_energy(-39.69881309)
    e1 = convert_energy(e1)
    e2 = convert_energy(e2)
    dx = dx*1e-10 
    # Constants. 
    mass = 14.007 * 1.661e-27
    light_speed = 299792458
    # Do calculation. 
    fac1 = (e1 - 2.*e0 + e2)/(dx**2.)
    fac2 = 2./mass
    freq = ma.sqrt(fac1*fac2)
    freq /= ma.pi
    wave_num = freq/light_speed
    # Convert to cm. 
    wave_num /= 100
    # Return. 
    return wave_num



def main():
    inputs = [(-39.69820674, -39.69858898, .05),
              (-39.68627560, -39.68941142, .1),
              (-39.66475440, -39.67541577, .15)]
    for e1, e2, dx in inputs:
        print(comp_wave_num(e1, e2, dx))
    return None


if __name__ == "__main__":
    main()